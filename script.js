var monTama;
var boutonManger = document.querySelector("#boutonManger");
boutonManger.style.display = "none";

class Tamagotchi {
  _nom;
  _genre;
  _barreDeVie;

  constructor(nom, genre) {
    this._nom = nom;
    this._genre = genre;
    this._barreDeVie = 100;

    console.log("monTama: ", nom, genre);
    this.init();
  }
  init() {
    var img = document.createElement("img");
    if (this._genre == "feminin") {
      img.src = "image-tamagotchi/lapin.png";
    }
    else if (this._genre == "masculin") {
      img.src = "image-tamagotchi/lapin-tama1.png";
    }
    var container = document.querySelector("container");
    container.appendChild(img);

    setInterval("perdreDeLaVie()", 1000);
  }

  set barreDeVie(value) {
    this._barreDeVie = value;
  }

  get barreDeVie() {
    return this._barreDeVie;

  }
  manger() {
    
  }
}

boutonEnvoyer.addEventListener("click", creatTamagotchi);

function creatTamagotchi() {
  event.preventDefault();

  let inputNom = document.querySelector("#nom");
  let inputGenre = document.querySelector("#genre");

  let nom = inputNom.value;
  let genre = inputGenre.value;

  if (nom == "") {
    console.error("Attention : pas de nom");
    return;
  }

  if (genre != "feminin" && genre != "masculin") {
    console.error("Attention : pas de genre");
    return;
  }

  monTama = new Tamagotchi(nom, genre);

  let formulaire = document.querySelector("form");
  formulaire.style.display = "none";

  boutonManger.style.display = "block";
  boutonManger.addEventListener("click", monTama.manger);
}

function perdreDeLaVie() {
  monTama.barreDeVie -= 1;

  if (monTama.barreDeVie == 0) {
    console.error("GAME OVER");
  }
}

